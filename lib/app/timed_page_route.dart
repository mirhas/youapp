import 'package:flutter/material.dart';
import 'package:youapp/app/constants.dart';

class TimedPageRoute extends MaterialPageRoute {
  TimedPageRoute({builder, time}) : super(builder: builder);

  @override
  Duration get transitionDuration => kTimedPageRouteDuration;
}
