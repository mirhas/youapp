import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:youapp/app/bouncing_scroll_behavior.dart';
import 'package:youapp/app/modules/pages.dart';
import 'package:youapp/app/theme/themes.dart';

import 'routes.dart';

class App extends StatelessWidget {
  const App({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'YouApp',
      builder: (context, child) => ScrollConfiguration(
        behavior: const BouncingScrollBehavior(),
        child: child!,
      ),
      theme: kTheme,
      darkTheme: kDarkTheme,
      themeMode: ThemeMode.dark,
      initialBinding: AuthBinding(),
      home: const AuthPage(),
      getPages: kGetPages,
    );
  }
}
