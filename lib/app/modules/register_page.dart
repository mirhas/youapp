import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:youapp/app/constants.dart';
import 'package:youapp/app/palette.dart';
import 'package:youapp/app/shared/back_text_button.dart';
import 'package:youapp/app/shared/gradient_button.dart';
import 'package:youapp/app/shared/gradient_text_button.dart';

class RegisterPage extends StatefulWidget {
  static const routeName = '/register';

  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leadingWidth: 80.0,
        leading: const BackTextButton(),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: kBackgroundGradient,
            ),
          ),
          ListView(
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.only(
              top: kToolbarHeight * 2,
              left: 18.0,
              right: 18.0,
            ),
            children: [
              const SizedBox(height: 18.0),
              Padding(
                padding: kHorizontalPadding,
                child: Text(
                  'Register',
                  style: textTheme.titleLarge,
                ),
              ),
              const SizedBox(height: 18.0),
              Container(
                padding: kHorizontalPadding,
                decoration: BoxDecoration(
                  color: kGunmetal,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.only(bottom: 16.0),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    label: Text('Enter Email'),
                    border: InputBorder.none,
                    hintText: 'johndoe@example.com',
                  ),
                ),
              ),
              const SizedBox(height: 18.0),
              Container(
                padding: kHorizontalPadding,
                decoration: BoxDecoration(
                  color: kGunmetal,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: TextFormField(
                  controller: _usernameController,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.only(bottom: 16.0),
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    label: Text('Enter username'),
                    border: InputBorder.none,
                    hintText: 'johndoe123',
                  ),
                ),
              ),
              const SizedBox(height: 18.0),
              Container(
                padding: kHorizontalPadding,
                decoration: BoxDecoration(
                  color: kGunmetal,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: TextFormField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    contentPadding: const EdgeInsets.only(bottom: 16.0),
                    label: const Text('Enter Password'),
                    border: InputBorder.none,
                    suffixIcon: IconButton(
                      onPressed: () {
                        // TODO: password visibility
                        print('visibility');
                      },
                      icon: SvgPicture.asset(kVisibilityOff),
                    ),
                  ),
                  obscureText: true,
                ),
              ),
              const SizedBox(height: 18.0),
              Container(
                padding: kHorizontalPadding,
                decoration: BoxDecoration(
                  color: kGunmetal,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: TextFormField(
                  controller: _confirmController,
                  decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    contentPadding: const EdgeInsets.only(bottom: 16.0),
                    label: const Text('Confirm Password'),
                    border: InputBorder.none,
                    suffixIcon: IconButton(
                      onPressed: () {
                        // TODO: password visibility
                        print('visibility');
                      },
                      icon: SvgPicture.asset(kVisibilityOff),
                    ),
                  ),
                  obscureText: true,
                ),
              ),
              const SizedBox(height: 18.0 * 2),
              GradientButton(
                // onPressed: credentialController.text.isEmpty &&
                //         passwordController.text.isEmpty
                //     ? null
                //     : () => Navigator.of(context).pushNamed('/profile'),
                onPressed: () => Navigator.of(context).pushNamed('/profile'),
                text: 'Login',
              ),
              const SizedBox(height: 18.0 * 3),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Have an account?\t'),
                  GradientTextButton(
                    underlined: true,
                    text: 'Login here',
                    onTap: () => Navigator.of(context).pop(),
                    gradient: kGoldGradient,
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
    _confirmController.dispose();
    super.dispose();
  }
}
