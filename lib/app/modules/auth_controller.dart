import 'package:get/get.dart';

import '../utils.dart';

class AuthController extends GetxController with CacheManager {
  @override
  void onInit() {
    super.onInit();
    auth();
  }

  Future<void> auth() async {
    await Future.delayed(const Duration(seconds: 2), () {});

    final token = getToken();

    if (token != null) {
      Get.offAndToNamed('/profile');
      return;
    }

    Get.offAndToNamed('/login');
  }
}
