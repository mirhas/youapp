import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../utils.dart';

enum CredentialType { username, email }

class LoginController extends GetxController with CacheManager {
  var credential = ''.obs;
  var password = ''.obs;
  final credentialController = TextEditingController();
  final passwordController = TextEditingController();

  void onCredentialChanged(String value) {
    credential.value = value;
  }

  void onPasswordChanged(String value) {
    password.value = value;
  }

  Future<void> login(String credential, String password) async {
    final queryParameters = {
      'email': credential,
      'password': password,
    };

    final url = Uri.parse('http://192.168.123.50:3000/auth/login');

    final headers = {HttpHeaders.contentTypeHeader: 'application/json'};

    final res = await http.post(
      url,
      headers: headers,
      body: json.encode(queryParameters),
    );

    final data = json.decode(res.body);

    if (res.statusCode == 201) {
      final token = data['token'];

      await saveToken(token);

      Get.offAllNamed('/profile');
    }
  }
}
