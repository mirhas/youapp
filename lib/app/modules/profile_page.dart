import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:youapp/app/constants.dart';
import 'package:youapp/app/palette.dart';
import 'package:youapp/app/shared/back_text_button.dart';
import 'package:youapp/app/shared/gradient_text_button.dart';

class ProfilePage extends StatefulWidget {
  static const routeName = '/profile';

  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final Set<String> _interest = {'Music', 'Basketball', 'Fitness', 'Gymming'};
  final String _about = 'n/a';
  bool _editAbout = false;
  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    return Scaffold(
      backgroundColor: kRichBlack,
      appBar: AppBar(
        leadingWidth: 80.0,
        leading: const BackTextButton(),
        centerTitle: true,
        title: Text(
          '@johndoe',
          style: textTheme.titleSmall,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.more_horiz_rounded),
            tooltip: 'More',
          ),
        ],
      ),
      body: ListView(
        padding: kHorizontalPadding,
        children: [
          Stack(
            children: [
              Container(
                alignment: Alignment.bottomLeft,
                padding: const EdgeInsets.all(12.0),
                height: 160.0,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: kGunmetal,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: const Text(
                  '@johndoe123,',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Positioned(
                top: 2.0,
                right: 2.0,
                child: Material(
                  clipBehavior: Clip.antiAlias,
                  shape: const CircleBorder(),
                  color: Colors.transparent,
                  child: IconButton(
                    onPressed: () {},
                    icon: SvgPicture.asset(kEdit),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 18.0),
          if (!_editAbout)
            Stack(
              children: [
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(
                      vertical: 12.0, horizontal: 18.0),
                  decoration: BoxDecoration(
                    color: kOnRichBlack,
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'About',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 28.0),
                      Text(
                        _about == 'n/a' ? kAbout : _about,
                        style: const TextStyle(color: kPaynesGray),
                      ),
                      const SizedBox(height: 12.0),
                    ],
                  ),
                ),
                Positioned(
                  top: 2.0,
                  right: 2.0,
                  child: Material(
                    color: Colors.transparent,
                    clipBehavior: Clip.antiAlias,
                    shape: const CircleBorder(),
                    child: IconButton(
                      onPressed: () {
                        // TODO: open edit about
                        setState(() => _editAbout = true);
                      },
                      icon: SvgPicture.asset(kEdit),
                    ),
                  ),
                ),
              ],
            ),
          if (_editAbout)
            Stack(
              children: [
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(
                      vertical: 12.0, horizontal: 18.0),
                  decoration: BoxDecoration(
                    color: kOnRichBlack,
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'About',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 28.0),
                      Row(
                        children: [
                          Material(
                            clipBehavior: Clip.antiAlias,
                            color: kOnGunmetal,
                            shape: ContinuousRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            child: IconButton(
                              onPressed: () {},
                              icon: SvgPicture.asset(
                                kAdd,
                              ),
                            ),
                          ),
                          const SizedBox(width: 12.0),
                          const Text('Add image'),
                        ],
                      ),
                      const SizedBox(height: 28.0),
                      Row(
                        children: [
                          const Text('Display name:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: 'Enter name',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                      Row(
                        children: [
                          const Text('Gender:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: 'Enter a gender',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                      Row(
                        children: [
                          const Text('Birthday:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: 'Enter a birthday',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                      Row(
                        children: [
                          const Text('Horoscope:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: 'Virgo',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                      Row(
                        children: [
                          const Text('Zodiac:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: 'Pig',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                      Row(
                        children: [
                          const Text('Height:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: '175 cm',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                      Row(
                        children: [
                          const Text('Weight:\t'),
                          Flexible(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: kOnGunmetal,
                                  borderRadius: kBorderRadiusCircular,
                                  border: Border.all(color: kPaynesGray)),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 12.0),
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hintText: '70 kg',
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 12.0),
                    ],
                  ),
                ),
                Positioned(
                  top: 12.0,
                  right: 18.0,
                  child: GradientTextButton(
                    text: 'Save & Update',
                    onTap: () {
                      // TODO: save, update, and close edit about
                      setState(() => _editAbout = false);
                    },
                    gradient: kGoldGradient,
                    underlined: false,
                  ),
                ),
              ],
            ),
          const SizedBox(height: 18.0),
          Stack(
            children: [
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 18.0),
                decoration: BoxDecoration(
                  color: kOnRichBlack,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Interest',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 28.0),
                    // Text(
                    //   _interest.isEmpty ? kInterest : _interest.first,
                    //   style: const TextStyle(color: kPaynesGray),
                    // ),
                    _buildInterest(
                      _interest,
                      const Color(0xff1c272c),
                      const TextStyle(
                        color: kPaynesGray,
                      ),
                    ),
                    const SizedBox(height: 12.0),
                  ],
                ),
              ),
              Positioned(
                top: 2.0,
                right: 2.0,
                child: Material(
                  color: Colors.transparent,
                  clipBehavior: Clip.antiAlias,
                  shape: const CircleBorder(),
                  child: IconButton(
                    onPressed: () => _onEditInterest(),
                    icon: SvgPicture.asset(kEdit),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 12.0),
        ],
      ),
    );
  }

  void _onEditInterest() {
    showModalBottomSheet(
      useSafeArea: false,
      isScrollControlled: true,
      context: context,
      builder: (context) {
        var textTheme = Theme.of(context).textTheme;

        return StatefulBuilder(
          builder: (context, setState) => Container(
            decoration: const BoxDecoration(
              gradient: kBackgroundGradient,
            ),
            height: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: kToolbarHeight),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const BackTextButton(),
                      TextButton(
                        onPressed: () {},
                        child: const Text('Save'),
                      ),
                    ],
                  ),
                  const SizedBox(height: kToolbarHeight),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0 * 2),
                    child: Text(
                      'Tell everyone about yourself',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        foreground: Paint()..shader = kLinearGradient,
                      ),
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0 * 2),
                    child: Text(
                      'What interest you?',
                      style: textTheme.titleLarge,
                    ),
                  ),
                  const SizedBox(height: 28.0),
                  Container(
                    margin: kHorizontalPadding,
                    width: double.infinity,
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                      color: const Color(0xff20353B),
                      borderRadius: kBorderRadiusCircular,
                    ),
                    child: _buildInterestChip(_interest),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildInterest(
    Set<String> interest,
    Color chipBackgroundColor,
    TextStyle emptyStateTextStyle,
  ) {
    if (interest.isEmpty) {
      return Text(kInterest, style: emptyStateTextStyle);
    }

    return Wrap(
      spacing: 8.0,
      runSpacing: 8.0,
      children: List.generate(
        interest.length,
        (index) => Container(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
          decoration: BoxDecoration(
            color: chipBackgroundColor,
            borderRadius: kBorderRadiusCircular,
          ),
          child: Text(interest.elementAt(index)),
        ),
      ),
    );
  }

  Widget _buildInterestChip(Set<String> interest) {
    if (interest.isEmpty) {
      return const SizedBox.shrink();
    }

    return Wrap(
      spacing: 8.0,
      children: List.generate(
        interest.length,
        (index) => Chip(
          onDeleted: () {
            print(interest.elementAt(index));
          },
          backgroundColor: const Color(0xff384d51),
          deleteIcon: const Icon(Icons.close_rounded),
          label: Text(
            interest.elementAt(index),
          ),
        ),
      ),
    );
  }
}
