import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:youapp/app/constants.dart';
import 'package:youapp/app/palette.dart';
import 'package:youapp/app/shared/gradient_button.dart';
import 'package:youapp/app/shared/gradient_text_button.dart';

import 'login_controller.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    return GetBuilder<LoginController>(
      builder: (controller) {
        return Obx(
          () => Scaffold(
            extendBodyBehindAppBar: true,
            appBar: AppBar(),
            body: Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                    gradient: kBackgroundGradient,
                  ),
                ),
                ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.only(
                    top: kToolbarHeight * 2,
                    left: 18.0,
                    right: 18.0,
                  ),
                  children: [
                    const SizedBox(height: 18.0),
                    Padding(
                      padding: kHorizontalPadding,
                      child: Text(
                        'Login',
                        style: textTheme.titleLarge,
                      ),
                    ),
                    const SizedBox(height: 18.0),
                    Container(
                      padding: kHorizontalPadding,
                      decoration: BoxDecoration(
                        color: kGunmetal,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      child: TextFormField(
                        controller: controller.credentialController,
                        onChanged: controller.onCredentialChanged,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.only(bottom: 16.0),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          label: Text('Enter Username/Email'),
                          border: InputBorder.none,
                          hintText: 'johndoe@example.com',
                        ),
                      ),
                    ),
                    const SizedBox(height: 18.0),
                    Container(
                      padding: kHorizontalPadding,
                      decoration: BoxDecoration(
                        color: kGunmetal,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      child: TextFormField(
                        controller: controller.passwordController,
                        onChanged: controller.onPasswordChanged,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          contentPadding: const EdgeInsets.only(bottom: 16.0),
                          label: const Text('Enter Password'),
                          border: InputBorder.none,
                          suffixIcon: IconButton(
                            onPressed: () {
                              // TODO: password visibility
                              print('visibility');
                            },
                            icon: SvgPicture.asset(kVisibilityOff),
                          ),
                        ),
                        obscureText: true,
                      ),
                    ),
                    const SizedBox(height: 18.0 * 2),
                    GradientButton(
                      onPressed: controller.credential.isEmpty ||
                              controller.password.isEmpty
                          ? null
                          : () {
                              controller.login(
                                controller.credential.value,
                                controller.password.value,
                              );
                            },
                      text: 'Login',
                    ),
                    const SizedBox(height: 18.0 * 3),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('No account?\t'),
                        GradientTextButton(
                          text: 'Register here',
                          underlined: true,
                          onTap: () {
                            // Get.toNamed('register');
                            print(controller.credentialController.value.text);
                          },
                          gradient: kGoldGradient,
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
