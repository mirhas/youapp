import 'package:flutter/material.dart';

// Color naming from https://coolors.co
const kMidnightGreen = Color(0xff1e3f44);
const kRichBlack = Color(0xff09141A);
const kOnRichBlack = Color(0xff0e191f);
const kCelestialBlue = Color(0xff4599db);
const kRobinEggBlue = Color(0xff62cdcb);
const kGoldGradient = LinearGradient(colors: [
  Color(0xffffe2be),
  Color(0xffd5be88),
  Color(0xff94783e),
  Color(0xffffe2be),
  Color(0xffd5be88),
]);
const kIndigoDye = Color(0xff214860);
const kDarkSlateGray = Color(0xff285457);
const kDisabledButtonGradient = LinearGradient(
  begin: Alignment.topRight,
  end: Alignment.bottomLeft,
  colors: [
    kIndigoDye,
    kDarkSlateGray,
  ],
);
const kEnableButtonGradient = LinearGradient(
  begin: Alignment.topRight,
  end: Alignment.bottomLeft,
  colors: [
    kCelestialBlue,
    kRobinEggBlue,
  ],
);
const kPaynesGray = Color(0xff59686b);
const kOnGunmetal = Color(0xff1a252a);
const kGunmetal = Color(0xff1c2d33);
const kBackgroundGradient = RadialGradient(
  center: Alignment.topRight,
  radius: 2,
  colors: [
    kMidnightGreen,
    kRichBlack,
  ],
);

final Shader kLinearGradient = kGoldGradient.createShader(
  const Rect.fromLTWH(
    0.0,
    0.0,
    200.0,
    70.0,
  ),
);
