import 'package:flutter/material.dart';

class BackTextButton extends StatelessWidget {
  const BackTextButton({super.key});

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      onPressed: () => Navigator.of(context).pop(),
      style: TextButton.styleFrom(
        foregroundColor: Colors.white,
      ),
      icon: const Icon(Icons.arrow_back_ios_new_rounded),
      label: const Text('Back'),
    );
  }
}
