import 'package:flutter/material.dart';
import 'package:youapp/app/palette.dart';

class GradientButton extends StatelessWidget {
  const GradientButton({
    super.key,
    required this.text,
    this.onPressed,
  });

  final VoidCallback? onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: onPressed != null
            ? [
                const BoxShadow(
                  color: kRobinEggBlue,
                  offset: Offset(
                    0.0,
                    5.0,
                  ),
                  spreadRadius: 0.0,
                  blurRadius: 12.0,
                ),
              ]
            : null,
      ),
      child: RawMaterialButton(
        onPressed: onPressed,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Ink(
          width: double.infinity,
          height: 45.0,
          decoration: BoxDecoration(
            gradient: onPressed == null
                ? kDisabledButtonGradient
                : kEnableButtonGradient,
          ),
          child: Center(
            child: Text(
              'Login',
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: onPressed == null ? kPaynesGray : Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
