import 'package:flutter/material.dart';

class GradientTextButton extends StatelessWidget {
  const GradientTextButton({
    required this.text,
    this.onTap,
    super.key,
    required this.gradient,
    this.underlined = true,
  });

  final VoidCallback? onTap;
  final String text;
  final Gradient gradient;
  final bool underlined;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ShaderMask(
        blendMode: BlendMode.srcIn,
        shaderCallback: (bounds) => gradient.createShader(
          Rect.fromLTWH(0, 0, bounds.width, bounds.height),
        ),
        child: Text(
          text,
          style: TextStyle(
              decoration:
                  underlined ? TextDecoration.underline : TextDecoration.none),
        ),
      ),
    );
  }
}
