import 'package:get/get.dart';

import 'modules/pages.dart';

final List<GetPage> kGetPages = [
  GetPage(
    name: '/',
    page: () => const AuthPage(),
    binding: AuthBinding(),
  ),
  GetPage(
    name: '/login',
    page: () => const LoginPage(),
    binding: LoginBinding(),
  ),
  GetPage(
    name: '/register',
    page: () => const RegisterPage(),
    binding: RegisterBinding(),
  ),
  GetPage(
    name: '/profile',
    page: () => const ProfilePage(),
    binding: ProfileBinding(),
  ),
];
