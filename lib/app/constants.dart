import 'package:flutter/material.dart';

const kTimedPageRouteDuration = Duration(milliseconds: 500);
const kHorizontalPadding = EdgeInsets.symmetric(horizontal: 18.0);
final kBorderRadiusCircular = BorderRadius.circular(12.0);

// Assets
const kVisibilityOff = 'svg/visibility_off.svg';
const kEdit = 'svg/edit.svg';
const kAdd = 'svg/add.svg';

// Placeholder or empty state
const kAbout = 'Add in your about to help others know you better';
const kInterest = 'Add in your interest to find a better match';
