// Mocks generated by Mockito 5.3.2 from annotations
// in youapp/test/login_unit_test.dart.
// Do not manually edit this file.

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i5;
import 'dart:ui' as _i7;

import 'package:flutter/material.dart' as _i3;
import 'package:get/get.dart' as _i2;
import 'package:get/get_state_manager/src/simple/list_notifier.dart' as _i6;
import 'package:mockito/mockito.dart' as _i1;
import 'package:youapp/app/modules/login_controller.dart' as _i4;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types
// ignore_for_file: subtype_of_sealed_class

class _FakeRxString_0 extends _i1.SmartFake implements _i2.RxString {
  _FakeRxString_0(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

class _FakeTextEditingController_1 extends _i1.SmartFake
    implements _i3.TextEditingController {
  _FakeTextEditingController_1(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

class _FakeInternalFinalCallback_2<T> extends _i1.SmartFake
    implements _i2.InternalFinalCallback<T> {
  _FakeInternalFinalCallback_2(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

/// A class which mocks [LoginController].
///
/// See the documentation for Mockito's code generation for more information.
class MockLoginController extends _i1.Mock implements _i4.LoginController {
  @override
  _i2.RxString get credential => (super.noSuchMethod(
        Invocation.getter(#credential),
        returnValue: _FakeRxString_0(
          this,
          Invocation.getter(#credential),
        ),
        returnValueForMissingStub: _FakeRxString_0(
          this,
          Invocation.getter(#credential),
        ),
      ) as _i2.RxString);
  @override
  set credential(_i2.RxString? _credential) => super.noSuchMethod(
        Invocation.setter(
          #credential,
          _credential,
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i2.RxString get password => (super.noSuchMethod(
        Invocation.getter(#password),
        returnValue: _FakeRxString_0(
          this,
          Invocation.getter(#password),
        ),
        returnValueForMissingStub: _FakeRxString_0(
          this,
          Invocation.getter(#password),
        ),
      ) as _i2.RxString);
  @override
  set password(_i2.RxString? _password) => super.noSuchMethod(
        Invocation.setter(
          #password,
          _password,
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i3.TextEditingController get credentialController => (super.noSuchMethod(
        Invocation.getter(#credentialController),
        returnValue: _FakeTextEditingController_1(
          this,
          Invocation.getter(#credentialController),
        ),
        returnValueForMissingStub: _FakeTextEditingController_1(
          this,
          Invocation.getter(#credentialController),
        ),
      ) as _i3.TextEditingController);
  @override
  _i3.TextEditingController get passwordController => (super.noSuchMethod(
        Invocation.getter(#passwordController),
        returnValue: _FakeTextEditingController_1(
          this,
          Invocation.getter(#passwordController),
        ),
        returnValueForMissingStub: _FakeTextEditingController_1(
          this,
          Invocation.getter(#passwordController),
        ),
      ) as _i3.TextEditingController);
  @override
  _i2.InternalFinalCallback<void> get onStart => (super.noSuchMethod(
        Invocation.getter(#onStart),
        returnValue: _FakeInternalFinalCallback_2<void>(
          this,
          Invocation.getter(#onStart),
        ),
        returnValueForMissingStub: _FakeInternalFinalCallback_2<void>(
          this,
          Invocation.getter(#onStart),
        ),
      ) as _i2.InternalFinalCallback<void>);
  @override
  _i2.InternalFinalCallback<void> get onDelete => (super.noSuchMethod(
        Invocation.getter(#onDelete),
        returnValue: _FakeInternalFinalCallback_2<void>(
          this,
          Invocation.getter(#onDelete),
        ),
        returnValueForMissingStub: _FakeInternalFinalCallback_2<void>(
          this,
          Invocation.getter(#onDelete),
        ),
      ) as _i2.InternalFinalCallback<void>);
  @override
  bool get initialized => (super.noSuchMethod(
        Invocation.getter(#initialized),
        returnValue: false,
        returnValueForMissingStub: false,
      ) as bool);
  @override
  bool get isClosed => (super.noSuchMethod(
        Invocation.getter(#isClosed),
        returnValue: false,
        returnValueForMissingStub: false,
      ) as bool);
  @override
  bool get hasListeners => (super.noSuchMethod(
        Invocation.getter(#hasListeners),
        returnValue: false,
        returnValueForMissingStub: false,
      ) as bool);
  @override
  int get listeners => (super.noSuchMethod(
        Invocation.getter(#listeners),
        returnValue: 0,
        returnValueForMissingStub: 0,
      ) as int);
  @override
  void onCredentialChanged(String? value) => super.noSuchMethod(
        Invocation.method(
          #onCredentialChanged,
          [value],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void onPasswordChanged(String? value) => super.noSuchMethod(
        Invocation.method(
          #onPasswordChanged,
          [value],
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i5.Future<void> login(
    String? credential,
    String? password,
  ) =>
      (super.noSuchMethod(
        Invocation.method(
          #login,
          [
            credential,
            password,
          ],
        ),
        returnValue: _i5.Future<void>.value(),
        returnValueForMissingStub: _i5.Future<void>.value(),
      ) as _i5.Future<void>);
  @override
  void update([
    List<Object>? ids,
    bool? condition = true,
  ]) =>
      super.noSuchMethod(
        Invocation.method(
          #update,
          [
            ids,
            condition,
          ],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void onInit() => super.noSuchMethod(
        Invocation.method(
          #onInit,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void onReady() => super.noSuchMethod(
        Invocation.method(
          #onReady,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void onClose() => super.noSuchMethod(
        Invocation.method(
          #onClose,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void $configureLifeCycle() => super.noSuchMethod(
        Invocation.method(
          #$configureLifeCycle,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i6.Disposer addListener(_i6.GetStateUpdate? listener) => (super.noSuchMethod(
        Invocation.method(
          #addListener,
          [listener],
        ),
        returnValue: () {},
        returnValueForMissingStub: () {},
      ) as _i6.Disposer);
  @override
  void removeListener(_i7.VoidCallback? listener) => super.noSuchMethod(
        Invocation.method(
          #removeListener,
          [listener],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void refresh() => super.noSuchMethod(
        Invocation.method(
          #refresh,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void refreshGroup(Object? id) => super.noSuchMethod(
        Invocation.method(
          #refreshGroup,
          [id],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void notifyChildrens() => super.noSuchMethod(
        Invocation.method(
          #notifyChildrens,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void removeListenerId(
    Object? id,
    _i7.VoidCallback? listener,
  ) =>
      super.noSuchMethod(
        Invocation.method(
          #removeListenerId,
          [
            id,
            listener,
          ],
        ),
        returnValueForMissingStub: null,
      );
  @override
  void dispose() => super.noSuchMethod(
        Invocation.method(
          #dispose,
          [],
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i6.Disposer addListenerId(
    Object? key,
    _i6.GetStateUpdate? listener,
  ) =>
      (super.noSuchMethod(
        Invocation.method(
          #addListenerId,
          [
            key,
            listener,
          ],
        ),
        returnValue: () {},
        returnValueForMissingStub: () {},
      ) as _i6.Disposer);
  @override
  void disposeId(Object? id) => super.noSuchMethod(
        Invocation.method(
          #disposeId,
          [id],
        ),
        returnValueForMissingStub: null,
      );
  @override
  _i5.Future<bool> saveToken(String? token) => (super.noSuchMethod(
        Invocation.method(
          #saveToken,
          [token],
        ),
        returnValue: _i5.Future<bool>.value(false),
        returnValueForMissingStub: _i5.Future<bool>.value(false),
      ) as _i5.Future<bool>);
  @override
  _i5.Future<void> removeToken() => (super.noSuchMethod(
        Invocation.method(
          #removeToken,
          [],
        ),
        returnValue: _i5.Future<void>.value(),
        returnValueForMissingStub: _i5.Future<void>.value(),
      ) as _i5.Future<void>);
}
