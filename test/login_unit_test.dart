import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'package:youapp/app/modules/login_controller.dart';

@GenerateNiceMocks([MockSpec<LoginController>()])
import 'login_unit_test.mocks.dart';

void main() {
  test('Auth test', () {
    var loginControllerMock = MockLoginController();

    when(loginControllerMock.login('johndoe@example.com', '123456'))
        .thenAnswer((_) async {
      return Future.value();
    });
  });
}
